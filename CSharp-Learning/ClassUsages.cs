﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Learning
{
    class ClassUsages
    {
        // Auto-implemented properties for trivial get and set
        public double TotalPurchases { get; set; }
        public string Name { get; set; }
        public int CustomerId { get; set; }

        private double Socre { get; set; }

        // Constructor
        public ClassUsages(double purchases, string name, int id, double socre)
        {
            TotalPurchases = purchases;
            Name = name;
            CustomerId = id;
            Socre = socre;
        }

        // Methods
        public string GetContactInfo() { return "ContactInfo"; }
        public string GetTransactionHistory() { return "History"; }

        // .. Additional methods, events, etc.
    }
}
