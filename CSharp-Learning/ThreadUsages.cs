﻿using System;
using System.Threading;

namespace CSharp_Learning
{
    class ThreadUsages
    {
        // 创建一个函数
        static void WriteY()
        {
            for (int i = 0; i < 10; ++i)
                Console.WriteLine("Y");
        }

        static void SleepThread()
        {
            
        }

    static public void ThreadDemo1()
        {
            // 创建一个线程，并绑定线程函数
            Thread t = new Thread(WriteY);
            t.Name = "ThreadDemo1";

            t.Start();
            
            for (int i = 0; i < 10; ++i)
                Console.WriteLine("X");

        }

        static public void ThreadDemo2()
        {
            // 创建一个线程，并绑定线程函数
            Thread t = new Thread(WriteY);

            t.Name = "ThreadDemo2";

            t.Start();

            // 等待线程结束
            t.Join();
            Console.WriteLine(t.Name);
            Console.WriteLine("Thread has been ended.");
            Console.WriteLine(t.Name);
        }

        static public void ThreadDemo3()
        {
            // 创建一个线程，并绑定线程函数
            Thread t = new Thread(WriteY);

            t.Name = "ThreadDemo3";

            t.Start();
            Thread.Sleep(1000);
            // 等待线程结束
            t.Join();
            
            Console.WriteLine("Thread3 has been ended.");
        }

        static public void ThreadDemo4()
        {
            // 创建一个线程，并绑定线程函数
            Thread t = new Thread(WriteY);

            t.Name = "ThreadDemo4";

            t.Start();
            Thread.Sleep(1000);
            // 等待线程结束
            t.Join();

            bool blocked = (t.ThreadState & ThreadState.WaitSleepJoin) != 0;
            Console.WriteLine(blocked);
            Console.WriteLine("t.ThreadState: " + t.ThreadState);
            // ThreadState.WaitSleepJoin = 32
            Console.WriteLine("ThreadState.WaitSleepJoin: " + ThreadState.WaitSleepJoin);
            Console.WriteLine("Thread3 has been ended.");
        }
    }
}
