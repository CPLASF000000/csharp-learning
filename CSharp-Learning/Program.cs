﻿using System;
using System.Threading;
using System.Threading.Tasks;

using ClassLibrary;

// 需安装 System.Text.Json NuGet包
using System.Text.Json;
using System.Text.Json.Serialization;

namespace CSharp_Learning
{
    class Program
    {
        static void Test4CallCSharp()
        {
            Class cclass = new Class();
            int re = cclass.add(1, 1);
            Console.Write(re);
        }

        static void Test4CallCPP()
        {
            Test.test();
        }

        static void Test4Auto_implemented_properties()
        {
            // Intialize a new object.
            ClassUsages cust1 = new ClassUsages(4987.63, "Northwind", 90108, 99.99);

            // Modify a property.
            cust1.TotalPurchases += 499.99;

            Console.WriteLine(cust1.TotalPurchases);
            //Console.WriteLine(cust1.Soc);
        }

        async static void Test4Task()
        {
            int re = await TaskUsages.GetPrimesCountAsync(2, 1000000);
            Console.WriteLine(re);
        }

        static void Test4GetPrimesTasks()
        {
            Test4Task();
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(Thread.CurrentThread.Name);
                Thread.Sleep(1000);
            }
        }

        static void Test4WebSocket()
        {
            //string url = "ws://192.168.3.122:8080";
            string url = "ws://192.168.3.122:8080";
            WebSocket sensor = new WebSocket();

            Console.WriteLine(sensor.connected());

            sensor.connect(url);

            int deadline = 10 * 1000;
            int wait_time = 0;
            while (sensor.connected() == false)
            {
                Thread.Sleep(200);
                wait_time += 200;
                sensor.connect(url);

                if (sensor.connected())
                    break;
                else if (wait_time > deadline)
                {
                    Console.WriteLine("wait time out!");
                    break;
                }
            }

            WeatherForecastWithPOCOs weatherForecast = new WeatherForecastWithPOCOs();

            string jsonString = JsonSerializer.Serialize(weatherForecast);

            Console.WriteLine("The messages has been send.");
            sensor.sendString(jsonString);

            Console.WriteLine(sensor.connected());

            
        }

        

        static void Main(string[] args)
        {

            //int c = Test.test(a, b);
            //Console.WriteLine(c);
            //Test.test();

            //Test4CallCSharp();
            //Test4CallCPP();

            /* ------ 测试类的基本操作 ----- */
            //Test4Auto_implemented_properties();

            /* ------ 测试异常处理的基本操作 ----- */
            //ExceptionHandling.ExcepitonDemo();

            // 给主线程命名
            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = "MainThread";

            //Console.WriteLine(Thread.CurrentThread.Name);

            /* ------ 测试Json的序列化和反序列化 ----- */
            //JsonUsages.Test();

            /* ------ 测试线程的基本操作 ----- */
            //ThreadUsages.ThreadDemo1();
            //ThreadUsages.ThreadDemo2();
            //ThreadUsages.ThreadDemo3();
            //ThreadUsages.ThreadDemo4();

            //Console.WriteLine(Thread.CurrentThread.Name);

            /* ------ 测试线程的基本操作 ----- */
            //TaskUsages.TaskDemo();
            //TaskUsages.TaskWait();
            //TaskUsages.LongTaskDemo();
            //TaskUsages.TaskReturnValueDemo();
            //TaskUsages.PrimerNumberTaskDemo();
            //TaskUsages.TaskExceptionHandling();
            //TaskUsages.PrimerNumberTaskawaiter();
            //TaskUsages.TaskDelayAwaiterDemo();
            //TaskUsages.TaskDelayContinueDemo();
            //TaskUsages.DisplayPrimeCounts();
            //TaskUsages.PrimerNumberTaskasync();

            //Test4Task();
            //Test4GetPrimesTasks();

            /* ------ 测试WebSocket的基本操作 ----- */
            Test4WebSocket();


            Console.ReadKey();
        }
    }
}
