﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HinYeung
{
    class ResponseHeader
    {
        public string Action { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
    }

    class ResponePayload
    {
        public string Payload { get; set; }
        public string Type { get; set; }
        public int Cols { get; set; }
        public int Rows { get; set; }
    }
    class PayloadResponse : ResponseHeader
    {
        public List<ResponePayload> Payloads { get; set; }
    }

    class RequestHeader
    {
        public string Action { get; set; }
    }

    // 设置 Style
    class SetStyleRequest : RequestHeader
    {
        public SetStyleRequest()
        {
            Action = "SetStyle";
            DisplayBit = 8;
        }
        public DisplayStyle Style { get; set; }
        public DisplayColor Color { get; set; }
        public int DisplayBit { get; set; }
    }

    // 设置 Fov
    class SetFoVRequest : RequestHeader
    {
        public SetFoVRequest()
        {
            Action = "SetFoV";
        }

        public List<int> FoV { get; set; }
    }

      // 设置 Distance
    class SetDistanceRequest : RequestHeader
    {
        public SetDistanceRequest()
        {
            Action = "SetDistance";
        }

        public List<int> Distance { get; set; }
    }

    // 设置 实时曝光
    class SetLiveExposureRequest : RequestHeader
    {
        public SetLiveExposureRequest()
        {
            Action = "SetLiveExposure";
        }

        public uint Live { get; set; }
    }

    // 设置 触发曝光
    class SetExposureRequest : RequestHeader
    {
        public SetExposureRequest()
        {
            Action = "SetExposure";
        }

        public uint Exposure { get; set; }
        public uint Period { get; set; }
    }
    
    // 设置 亮度（电流）
    class SetCurrentRequest : RequestHeader
    {
        public SetCurrentRequest()
        {
            Action = "SetCurrent";
        }

        public List<byte> Current { get; set; }
    }

    // 触发拍照
    class CaptureRequest : RequestHeader
    {
        public CaptureRequest()
        {
            Action = "Capture";
        }

        public bool Image { get; set; }
    }

    // 开始计算
    class ComputeRequest : RequestHeader
    {
        public ComputeRequest()
        {
            Action = "Compute";
        }

        public string Payload { get; set; }
    }
}
