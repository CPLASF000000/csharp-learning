﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

// 需安装 System.Text.Json NuGet包
using System.Text.Json;
using System.Text.Json.Serialization;

namespace CSharp_Learning
{
    public class WeatherForecastWithPOCOs
    {
        public DateTimeOffset Date { get; set; } 
        public int TemperatureCelsius { get; set; }
        public string Summary { get; set; }
        public string SummaryField;
        public IList<DateTimeOffset> DatesAvailable { get; set; }
        public Dictionary<string, HighLowTemps> TemperatureRanges { get; set; }
        public string[] SummaryWords { get; set; }
    }

    public class HighLowTemps
    {
        public int High { get; set; }
        public int Low { get; set; }
    }
    class JsonUsages
    {
        public static void Test()
        {
            string path = System.IO.Directory.GetCurrentDirectory();
            Console.WriteLine(path);

            string fileName = path + "/test4Json.json";
            WeatherForecastWithPOCOs weatherForecast = new WeatherForecastWithPOCOs();


            string jsonString = JsonSerializer.Serialize(weatherForecast);

           
            File.WriteAllText(fileName, jsonString);
        }
    }
}
