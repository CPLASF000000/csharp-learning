﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.WebSockets;
using System.Text.Json;
using System.Text.Json.Serialization;
using OpenCvSharp;
using System.Threading;

namespace HinYeung
{
    public enum SensorType
    {
        Undefine,
        Monocular,
        Binocular
    }
    public enum DisplayStyle
    {
        Gray = 3,
        Color
    }

    public enum DisplayColor
    {
        NoColor = 0,
        Red = 1,
        Green = 2,
        Yellow = 3, // green + red
        Blue = 4,
        Magenta = 5, // blue + red
        Cyan = 6, // blue + green
        White = 7 // blue + green + red
    }

    public class HYSensorInfo
    {
        public string Name { get; set; }
        public string Model { get; set; }
        public string SerialNo { get; set; }
        public string Version { get; set; }
        public string Manufacturer { get; set; }
        public string ActiveDate { get; set; }
        public string Protocol { get; set; }
        public string ProtocolVersion { get; set; }
        public DisplayColor SupportColor { get; set; }
        public DisplayColor Color { get; set; }
        public DisplayStyle Style { get; set; }
        public SensorType Type { get; set; }
        public List<int> Resolution { get; set; }
        public int DecodeType { get; set; }
        public uint Exposure { get; set; }
        public uint Period { get; set; }
        public uint LiveExposure { get; set; }
        public List<byte> Current { get; set; }
        public List<int> FoV { get; set; }
        public List<int> Distance { get; set; }
    }

    //public delegate void LivePlaybackEventHandler(object sender, Mat image, int position);
    public delegate void CaptureEventHandler(object sender, List<Mat> images);
    public delegate void ComputeEventHandler(object sender, Mat color, Mat depth);

    public class HYSensor
    {
        private ClientWebSocket client = null;
        private CancellationTokenSource cancellation = new CancellationTokenSource();
        private int textLength = 1024;
        private int binaryLength = 1024;
        private List<List<byte>> binaryMessage = new List<List<byte>>();

        public HYSensor()
        {
            cancellation.CancelAfter(TimeSpan.FromSeconds(60));
            Info = new HYSensorInfo();
        }

        //public event LivePlaybackEventHandler onLivePlayback;
        public event CaptureEventHandler onCaptureSucess;
        public event ComputeEventHandler onComputeFinish;

        public HYSensorInfo Info { get; private set; }

        // connect函数
        public void Connect(Uri uri)
        {
            if (client != null 
                && (client.State == WebSocketState.Connecting || client.State == WebSocketState.Open))
                return;

            Task.Run(async () =>
            {
                try
                {
                    client = new ClientWebSocket();
                    await client.ConnectAsync(uri, cancellation.Token);
                }
                catch (Exception ex)
                {
                    client.Abort();
                    client.Dispose();
                    client = null;
                    throw ex;
                }
                
            }).Wait();
            ReceiveAsync(textLength);
        }

        public bool IsConnected()
        {
            if (client != null)
                return client.State == WebSocketState.Open;
            else
                return false;
        }

        public void Close()
        {
            Task.Run(async () =>
            {
                try
                {
                    await client.CloseAsync(WebSocketCloseStatus.NormalClosure, "Bye", CancellationToken.None);
                }
                catch (Exception ex)
                { }

                client.Abort();
                client.Dispose();
                client = null;
            }).Wait();
        }

        public void Style(DisplayStyle style = DisplayStyle.Gray, DisplayColor led = DisplayColor.Blue)
        {
            SetStyleRequest request = new SetStyleRequest();
            request.Style = style;
            request.Color = led;
            string json = JsonSerializer.Serialize(request);
            SendAsync(json);
            ReceiveAsync(textLength);
        }

        public void FoV(int x, int y, int width, int height)
        {
            SetFoVRequest request = new SetFoVRequest();
            request.FoV = new List<int>();
            int[] fov = { x, y, width, height };
            request.FoV.AddRange(fov);
            string json = JsonSerializer.Serialize(request);
            SendAsync(json);
            ReceiveAsync(textLength);
        }

        public void Distance(int min, int max)
        {
            SetDistanceRequest request = new SetDistanceRequest();
            request.Distance = new List<int>();
            int[] distance = { min, max };
            request.Distance.AddRange(distance);
            string json = JsonSerializer.Serialize(request);
            SendAsync(json);
            ReceiveAsync(textLength);
        }

        public void LiveExposure(uint us)
        {
            SetLiveExposureRequest request = new SetLiveExposureRequest();
            request.Live = us;
            string json = JsonSerializer.Serialize(request);
            SendAsync(json);
            ReceiveAsync(textLength);
        }

        public void Exposure(uint exposure, uint period = 0)
        {
            SetExposureRequest request = new SetExposureRequest();
            request.Exposure = exposure;
            request.Period = period;
            string json = JsonSerializer.Serialize(request);
            SendAsync(json);
            ReceiveAsync(textLength);
        }

        public void Current(byte red, byte green, byte blue)
        {
            SetCurrentRequest request = new SetCurrentRequest();
            request.Current = new List<byte>();
            byte[] current = { red, green, blue };
            request.Current.AddRange(current);
            string json = JsonSerializer.Serialize(request);
            SendAsync(json);
            ReceiveAsync(textLength);
        }

        public void Capture(bool image = false)
        {
            CaptureRequest request = new CaptureRequest();
            request.Image = image;
            string json = JsonSerializer.Serialize(request);
            if(image)
                binaryMessage.Clear();
            SendAsync(json);
            if(image)
            {
                ReceiveAsync(binaryLength);
                if (Info.Type == SensorType.Binocular)
                    ReceiveAsync(binaryLength);
            }
            ReceiveAsync(textLength);
        }

        public void Compute()
        {
            ComputeRequest request = new ComputeRequest();
            request.Payload = "Depth";
            string json = JsonSerializer.Serialize(request);
            binaryMessage.Clear();
            SendAsync(json);
            ReceiveAsync(binaryLength);
            ReceiveAsync(binaryLength);
            ReceiveAsync(textLength);
        }

        private void SendAsync(string json)
        {
            Task.Run(async () =>
            {
                await client.SendAsync(new ArraySegment<byte>(Encoding.UTF8.GetBytes(json)),
                    WebSocketMessageType.Text, true, CancellationToken.None);
            }).Wait();
        }
        
        private void ReceiveAsync(int length)
        {
            var buffer = new ArraySegment<byte>(new byte[length]);
            Task.Run(async () =>
            {
                var result = await client.ReceiveAsync(buffer, CancellationToken.None);
                List<byte> message = new List<byte>();
                while (!result.CloseStatus.HasValue)
                {
                    message.AddRange(buffer.Take(result.Count));
                    if (result.EndOfMessage)
                    {
                        switch(result.MessageType)
                        {
                            case WebSocketMessageType.Text:
                                ParseTextMessage(Encoding.UTF8.GetString(message.ToArray(), 0, message.Count));
                                break;
                            case WebSocketMessageType.Binary:
                                binaryMessage.Add(message);
                                break;
                            default:
                                Close();
                                break;
                        }
                        return;
                    }
                    result = await client.ReceiveAsync(buffer, CancellationToken.None);
                }
            }).Wait();
        }

        // 解析明文数据
        private void ParseTextMessage(string json)
        {
            ResponseHeader header = JsonSerializer.Deserialize<ResponseHeader>(json);
            if (header.Status != 0)
                throw new ApplicationException(header.Message);
            
            switch(header.Action)
            {
                case "Capture":
                case "Compute":
                    PayloadResponse response = JsonSerializer.Deserialize<PayloadResponse>(json);
                    if (response.Payloads != null && response.Payloads.Count == binaryMessage.Count)
                        ParseBinaryMessage(response);
                    break;
                default:
                    Info = JsonSerializer.Deserialize<HYSensorInfo>(json);
                    if (Info.Resolution != null)
                        binaryLength = Info.Resolution.First() * Info.Resolution.Last() * sizeof(float) * 3;
                    break;
            }            
        }

        // 解析二进制数据 => images
        private void ParseBinaryMessage(PayloadResponse response)
        {
            List<Mat> images = new List<Mat>();
            for(int i = 0; i < response.Payloads.Count; ++i)
            {
                if (response.Payloads[i].Payload != "Mat")
                    continue;

                MatType type = MatType.CV_8UC1;
                switch(response.Payloads[i].Type)
                {
                    case "CV_8UC1":
                        type = MatType.CV_8UC1;
                        break;
                    case "CV_8UC3":
                        type = MatType.CV_8UC3;
                        break;
                    case "CV_32FC3":
                        type = MatType.CV_32FC3;
                        break;
                    default:
                        new FormatException("unsupport mat type: " + response.Payloads[i].Type);
                        break;
                }
                images.Add(new Mat(response.Payloads[i].Rows, response.Payloads[i].Cols, type, binaryMessage[i].ToArray()));
            }
            if (response.Action == "Caputre")
                onCaptureSucess(this, images);
            else
                onComputeFinish(this, images.First(), images.Last());
        }
    }
}
