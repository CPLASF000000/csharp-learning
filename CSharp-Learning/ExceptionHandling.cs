﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Learning
{
    class ExceptionHandling
    {
        static void ExcepitonNull()
        {
            throw null;
        }

        static public void ExcepitonDemo()
        {
            try
            {
                ExcepitonNull();
            }
            catch (AggregateException error)
            {
                if (error.InnerExceptions is NullReferenceException)
                    Console.WriteLine("Catch a Error about null.");
                else
                    throw;
            }
        }
    }
}
