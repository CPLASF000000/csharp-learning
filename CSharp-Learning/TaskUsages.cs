﻿using System;
using System.Threading;
using System.Threading.Tasks;

using System.Linq;

namespace CSharp_Learning
{
    class TaskUsages
    {
        // 创建Task
        static public void CreateTask1()
        {
            var t1 = new Task(
                () => {
                    Console.WriteLine("任务开始工作……");
                    //模拟工作过程, 休眠5秒
                    Thread.Sleep(5000);
                }
            );

            t1.Start();
            
            Task.WaitAll(t1);
        }

        // 阻塞Task
        static public void TaskWait()
        {
            Task task = Task.Run(
                () => {
                    Console.WriteLine("任务开始工作……");
                    //模拟工作过程, 休眠5秒
                    Thread.Sleep(5000);
                }
            );

            //task.Start();
            Console.WriteLine(task.IsCompleted);
            task.Wait();

            Console.WriteLine(task.IsCompleted);
        }

        // Task 使用长任务，避免使用线程池
        static public void LongTaskDemo()
        {
            Task task = Task.Factory.StartNew(
                () => {
                    Thread.Sleep(2000);
                    Console.WriteLine("Run the Task.");
        
                },
                TaskCreationOptions.LongRunning
            );
            
        }

        // Task 的返回值
        static public void TaskReturnValueDemo()
        {
            Task<int> task = Task.Run(
                () => {
                    Console.WriteLine("Run the Task.");
                    Thread.Sleep(2000);
                    return 3;
                }
            );
            Console.WriteLine(task.Result);
        }

        // Task 的异常处理
        static public void TaskExceptionHandling()
        {
            Task task = Task.Run(
                () => throw null
            );
            try
            {
                task.Wait();
            }
            catch (AggregateException error)
            {
                if (error.InnerException is NullReferenceException)
                    Console.WriteLine("Catch the exception of null.");
                else
                    throw;
            }

            //Console.WriteLine(task.Result);
        }

        // Task 计算前三百万个数中质数的个数
        static public void PrimerNumberTaskDemo()
        {
            Task<int> task = Task.Run(
                () => Enumerable.Range(2, 3000000).Count(
                    n => Enumerable.Range(2, (int)Math.Sqrt(n) - 1).All(
                        i => n % i > 0
                    )
                )
            );

            Console.WriteLine("Task running...");
            Console.WriteLine("The answer is: " + task.Result);
        }

        // Task 计算前三百万个数中质数的个数 使用了延续技术
        static public void PrimerNumberTaskawaiter()
        {
            Task<int> task = Task.Run(
                () => Enumerable.Range(2, 3000000).Count(
                    n => Enumerable.Range(2, (int)Math.Sqrt(n) - 1).All(
                        i => n % i > 0
                    )
                )
            );

            var awaiter = task.GetAwaiter();
            awaiter.OnCompleted(
                () =>
                {
                    int re = awaiter.GetResult();
                    Console.WriteLine(re);
                }
            );

            Console.WriteLine("Task running...");
            Console.WriteLine("The answer is: " + task.Result);
        }

        // Task.Delay()
        static public void TaskDelayAwaiterDemo()
        {
            Task<int> task = Task.Run(
                () => {
                    Console.WriteLine("Run the Task.");

                    Task.Delay(5000).GetAwaiter().OnCompleted(
                        () => Console.WriteLine(42)
                    );

                    return 3;
                }
            );
            Console.WriteLine(task.Result);
        }

        // Task.Delay()
        static public void TaskDelayContinueDemo()
        {
            Task<int> task = Task.Run(
                () => {
                    Console.WriteLine("Run the Task.");

                    Task.Delay(5000).ContinueWith(
                        ant => Console.WriteLine(42)
                    );

                    return 3;
                }
            );
            Console.WriteLine(task.Result);
        }

        static int GetPrimesCount(int start, int count)
        {
            return
                ParallelEnumerable.Range(start, count).Count(
                    n => Enumerable.Range(2, (int)Math.Sqrt(n) - 1).All(i => n % i > 0));
        }

        static public void DisplayPrimeCounts()
        {
            for (int i = 0; i < 10; i++)
                Console.WriteLine(GetPrimesCount(i * 1000000 + 2, 1000000) +
                    " primes between " + (i * 1000000) + " and " + ((i + 1) * 1000000 - 1));
            Console.WriteLine("Done!");
        }

        static public Task<int> GetPrimesCountAsync(int start, int count)
        {
            return
                Task.Run(
                    () =>
                    ParallelEnumerable.Range(start, count).Count(
                        n => Enumerable.Range(2, (int)Math.Sqrt(n) - 1).All(i => n % i > 0))
                );
        }

       

        // Task 计算前三百万个数中质数的个数 使用了asybc 异步编程技术
        async static public Task PrimerNumberTaskasync()
        {
            for (int i = 0; i < 10; i++)
                Console.WriteLine(await GetPrimesCountAsync(i * 1000000 + 2, 1000000) +
                    " primes between " + (i * 1000000) + " and " + ((i + 1) * 1000000 - 1));
            Console.WriteLine("Done!");
        }

        

        static public void TaskDemo()
        {
            Task t = new Task(
                () =>
                {
                    Console.WriteLine("任务开始工作……");
                    //模拟工作过程, 休眠5秒
                    Thread.Sleep(5000);
                }
            );
            
            // 开始任务
            t.Start();

            // 任务完成时进行操作
            t.ContinueWith(
                (task) =>
                {
                    Console.WriteLine("任务完成，完成时候的状态为：");
                    Console.WriteLine("IsCanceled={0}\tIsCompleted={1}\tIsFaulted={2}", task.IsCanceled, task.IsCompleted, task.IsFaulted);
                }
            );
            Console.ReadKey();
        }


    }
}
