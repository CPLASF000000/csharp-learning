﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.Threading.Tasks;

using System.Web.Http;
using System.Net.WebSockets;

namespace CSharp_Learning
{

    class WebSocket
    {
        readonly ClientWebSocket _webSocket = new ClientWebSocket();
        readonly CancellationToken _cancellation = new CancellationToken();

        public bool connected_ { get; set; } = false;

        static public byte[] ByteCut(byte[] buf, byte cut = 0x00) {
            var list = new List<byte>();
            list.AddRange(buf);
            for (var i = list.Count - 1; i >= 0; i--) { 
                if (list[i] == cut) { 
                    list.RemoveAt(i);
                } 
                else {
                    break;
                } 
            } 
            var lastbyte = new byte[list.Count];
            for (var i = 0; i < list.Count; i++) {
                lastbyte[i] = list[i];
            } 
            return lastbyte; 
        }

        public async void connect(string url)
        {
            try
            {
                await _webSocket.ConnectAsync(new Uri(url), _cancellation);
                Console.WriteLine(_webSocket.State);
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                if (_webSocket.State != WebSocketState.Open)
                    connected_ = false;
            }
            //return _webSocket.State;
        }

        public bool connected()
        {
            if (_webSocket.State == WebSocketState.Open)
                return true;
            return false;
        }

        public void sendString()
        {
            string messages = "Hello, world!";
            Byte[] bsend = System.Text.Encoding.Default.GetBytes(messages);
            
            Console.WriteLine(BitConverter.ToString(bsend));

            _webSocket.SendAsync(new ArraySegment<byte>(bsend), WebSocketMessageType.Text, true, _cancellation);
        }

        public void sendString(string messages)
        {
            Byte[] bsend = System.Text.Encoding.Default.GetBytes(messages);

            Console.WriteLine(BitConverter.ToString(bsend));

            _webSocket.SendAsync(new ArraySegment<byte>(bsend), WebSocketMessageType.Text, true, _cancellation);
        }

        public async void WebSocketClient()
        {
            try
            {
                //建立连接
                var url = "ws://192.168.3.122:8080";

                await _webSocket.ConnectAsync(new Uri(url), _cancellation);

                var bsend = new byte[1024];

                await _webSocket.SendAsync(new ArraySegment<byte>(bsend), WebSocketMessageType.Binary, true, _cancellation); //发送数据

                while (true)
                {
                    var result = new byte[1024];

                    await _webSocket.ReceiveAsync(new ArraySegment<byte>(result), new CancellationToken());//接受数据

                    // 通过ByteCut方法来对byte数组进行裁剪？
                    var lastbyte = ByteCut(result, 0x00);

                    // 将一个byte数组解码为字符串
                    var str = Encoding.UTF8.GetString(lastbyte, 0, lastbyte.Length);

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
