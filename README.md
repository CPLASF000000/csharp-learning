# C#-Learning

这是一个关于C#的学习笔记库。

## CSharp-Learning 

项目里是C#的代码，也是这个vs工程的启动项目。

## ClassLibrary 

项目是一个C#类库（.Net framework），主要用来学习C#类库生成DLL的。

## DLL-CPP 

项目是一个C++ 动态链接库项目，主要是用来学习C#如何调用C++的。

## TaskUsages.cs

参考链接：https://www.cnblogs.com/zhaoshujie/p/11082753.html

## WebSocket

为了使用`System.Web.Http`命名空间，还得在Nuget包管理器里下载`Microsoft.AspNet.WebApi.Core`包


